## MobAd Lite SDK


## Getting Started

The following instructions will allow you to integrate the MobAd Lite SDK in your android app using android studio.

### Installation

**Step 1**. Add the JitPack repository to your build file
Add it in your root build.gradle at the end of repositories.

```
allprojects {
    repositories {
        ...
        maven { 
            url 'https://jitpack.io'
        }
    }
}
```

**Step 2**. In the app gradle file, add the line that sets the provided App ID and App Access Token to enable our server to identify and authenticate your app.

```
android {
    ...
    defaultConfig {
        ...
        manifestPlaceholders = [
            app_id          : "<your_app_id>",
            app_access_token: "<your_app_access_token>"
            ]
    }
}
```

**Step 3**. Add the dependency

```
dependencies {
    implementation 'com.gitlab.imw-developers:mobad-android-sdk-lite:x.x.x'
}
```

(You have to replace the `x.x.x` with the latest version of the SDK that you can find here: https://mobadpreprod.azurewebsites.net/sdk/latest_gradle_version)

Note:
>If you want to test it's preferable to do it with our dedicated for-testing servers, you'll have to add -testing to your version (Example: 1.0.0-testing).

### Usage:

Instantiate the MobAd class in Kotlin:

```
val mobAd = MobAd(activity)
```

or in Java

```
MobAd mobAd = new MobAd(activity);
```
---

### Update Ad Service Status:

This function allows users to update their ad service status by setting a boolean value indicating whether they have opted-in or opted-out of the service.

In order to enable ad service status in kotlin/Java:

```
mobAd.updateAdServiceStatus(/** your boolean goes here **/)
```

---

### Get Ad:
In order to get the ad in kotlin:

```
mobAd.getAd(object: AdCompletionListener {
    override fun onSuccess(ad: Ad) {
        //display the ad
    }

    override fun onError(message: String) {
        //Todo on failure
    }
})
```

or in Java

```
mobAd.getAd(new AdCompletionListener() {
    @Override
    public void onSuccess(Ad ad) {
        //display the ad
    }

    @Override
    public void onError(String message) {
        //Todo on error
    }
});
```

**OPTIONAL** You can add an adLanguage property to the getAd function as a string, its value is the two-letter code for a language eg `en fr ar`.

If you don't add a language, it will default to the language used within the app.

**Notes**:
* **Until now**, when you initially set the ad language, it will be stored; afterward, changing it will have no effect.
* You have to design different types of ads. 

---

### Send ad action:

Sending user's actions on the ad in kotlin:

```
 mobAd.sendAdAction(
    AdAction(
        "ACTION",
        ad.id,
        ad.actionTokens[n]
    )
)
```

or in Java

```
mobAd.sendAdAction(
    new AdAction(
        "ACTION",
        ad.getId(),
        ad.getActionTokens().get(n)
));
```

**Action keys** Scenarios:
* Coordinating with us to discuss scenarios for ad actions.

**Notes**: 
* `actionToken`: is any token from the token list you received inside the ad object.
* You have to send each user action that we agreed together to the server.

---

That's all you need to do, for further information contact us.