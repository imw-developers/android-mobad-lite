package com.imagineworks.mobad_sdk_lite

import com.imagineworks.mobad_sdk_lite.data.model.Ad

interface AdCompletionListener {
    fun onSuccess(ad: Ad)
    fun onError(message: String)
}