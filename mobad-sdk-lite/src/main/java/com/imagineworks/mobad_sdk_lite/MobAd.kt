package com.imagineworks.mobad_sdk_lite

import android.content.Context
import android.content.pm.PackageManager
import android.provider.Settings
import android.util.Log
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.coroutineScope
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.imagineworks.mobad_sdk_lite.data.model.*
import com.imagineworks.mobad_sdk_lite.data.repository.MobAdRepository
import com.imagineworks.mobad_sdk_lite.data.source.local.LocalDataSourceImpl
import com.imagineworks.mobad_sdk_lite.manager.ad.AdManager
import com.imagineworks.mobad_sdk_lite.manager.ad.AdManagerImpl
import com.imagineworks.mobad_sdk_lite.manager.user.UserManager
import com.imagineworks.mobad_sdk_lite.manager.user.UserManagerImpl
import com.imagineworks.mobad_sdk_lite.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.util.Locale

class MobAd(private val context: Context) {
    private val scope: LifecycleCoroutineScope? = context.getLifecycle()?.coroutineScope

    private val isNetworkAvailable
        get() = context.isNetworkAvailable()

    private val repository by lazy { MobAdRepository(LocalDataSourceImpl(context)) }
    private val userManager: UserManager by lazy { UserManagerImpl(repository) }
    private val adManager: AdManager by lazy { AdManagerImpl(repository) }

    fun updateAdServiceStatus(status: Boolean) {
        if (isNetworkAvailable) {
            scope?.launch(Dispatchers.IO) {
                if (userManager.isUserInitialized()) {
                    val userDataResponse = userManager.getCurrentUserResponse()
                    handleUserResponseToUpdateAdServiceStatus(userDataResponse, status)
                } else {
                    startInitUserThenUpdateAdServiceStatus(Locale.getDefault().language, status)
                }
            }
        } else {
            Log.e(TAG, NETWORK_UNREACHABLE)
        }
    }

    fun getAd(
        adCompletionListener: AdCompletionListener,
        adLanguage: String
    ) {
        if (isNetworkAvailable) {
            scope?.launch(Dispatchers.IO) {
                if (userManager.isUserInitialized()) {
                    val userDataResponse = userManager.getCurrentUserResponse()
                    handleUserResponseToGetAd(userDataResponse, adCompletionListener)
                } else
                    startInitUserThenGetAd(adLanguage, adCompletionListener)
            }
        } else {
            Log.e(TAG, NETWORK_UNREACHABLE)
            adCompletionListener.onError(NETWORK_UNREACHABLE)
        }
    }

    fun getAd(
        adCompletionListener: AdCompletionListener,
    ) {
        getAd(adCompletionListener, Locale.getDefault().language)
    }

    fun sendAdAction(adAction: AdAction) {
        if (isNetworkAvailable) {
            scope?.launch(Dispatchers.IO) {
                val userToken = userManager.getUserAccessToken()
                adManager.sendAdAction(userToken, adAction)
            }
        } else {
            Log.e(TAG, NETWORK_UNREACHABLE)
        }
    }

    private suspend fun startInitUserThenGetAd(
        adLanguage: String,
        adCompletionListener: AdCompletionListener
    ) {
        prepareInitUserRequest(adLanguage, { user: User ->
            handleUserResponseToGetAd(
                DataResponse.SuccessResponse(user),
                adCompletionListener
            )
        }) { errorMessage: String ->
            adCompletionListener.onError(errorMessage)
        }
    }

    private suspend fun startInitUserThenUpdateAdServiceStatus(
        adLanguage: String,
        status: Boolean
    ) {
        prepareInitUserRequest(adLanguage, { user: User ->
            handleUserResponseToUpdateAdServiceStatus(
                DataResponse.SuccessResponse(user),
                status
            )
        }) { errorMessage: String ->
            Log.e(TAG, errorMessage)
        }
    }

    private fun handleUserResponseToGetAd(
        response: DataResponse<User>,
        adCompletionListener: AdCompletionListener
    ) {
        when (response) {
            is DataResponse.FailureResponse -> adCompletionListener.onError(response.errorString)
            is DataResponse.SuccessResponse -> getAdFromAdManager(
                response.data,
                adCompletionListener
            )
        }
    }

    private fun handleUserResponseToUpdateAdServiceStatus(
        response: DataResponse<User>,
        status: Boolean
    ) {
        when (response) {
            is DataResponse.FailureResponse -> Log.e(TAG, response.errorString)
            is DataResponse.SuccessResponse -> updateAdServiceStatusFromUserManager(
                response.data,
                status
            )
        }
    }

    private fun getAdFromAdManager(
        user: User,
        adCompletionListener: AdCompletionListener
    ) {
        scope?.launch(Dispatchers.IO) {
            val response = async { adManager.getAd(user) }
            scope?.launch(Dispatchers.Main) {
                when (val adResponse = response.await()) {
                    is DataResponse.FailureResponse -> adCompletionListener.onError(adResponse.errorString)
                    is DataResponse.SuccessResponse -> adCompletionListener.onSuccess(adResponse.data)
                }
            }
        }
    }

    private fun updateAdServiceStatusFromUserManager(
        user: User,
        status: Boolean
    ) {
        scope?.launch(Dispatchers.IO) {
            val response = async { userManager.updateAdServiceStatus(user, status) }

            when (val adStatusResponse = response.await()) {
                is DataResponse.FailureResponse -> Log.e(TAG, adStatusResponse.errorString)
                is DataResponse.SuccessResponse -> Log.i(TAG, adStatusResponse.data.toString())
            }

        }
    }

    private suspend fun prepareInitUserRequest(
        adLanguage: String,
        onSuccess: (User) -> Unit,
        onError: (String) -> Unit
    ) {
        try {
            scope?.launch(Dispatchers.IO) {
                val adInfo = async { AdvertisingIdClient.getAdvertisingIdInfo(context) }
                completeInitUserRequest(adLanguage, adInfo.await().id, onSuccess, onError)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            completeInitUserRequest(adLanguage, null, onSuccess, onError)
        }
    }

    private suspend fun completeInitUserRequest(
        adLanguage: String,
        idfa: String?,
        onSuccess: (User) -> Unit,
        onError: (String) -> Unit
    ) {
        val userDataResponse = userManager.initUser(
            getIdentifier(Identifier.APP_ACCESS_TOKEN),
            InitUserRequest(
                idfa ?: getIdentifier(Identifier.IDFA),
                getIdentifier(Identifier.IDFV),
                getIdentifier(Identifier.APP_ID),
                getIdentifier(Identifier.COUNTRY_CODE),
                adLanguage
            )
        )
        when (userDataResponse) {
            is DataResponse.FailureResponse -> onError(userDataResponse.errorString)
            is DataResponse.SuccessResponse -> onSuccess(userDataResponse.data)
        }
    }

    private fun getIdentifier(identifier: Identifier): String {
        return when (identifier) {
            Identifier.APP_ACCESS_TOKEN -> getGradleIdentifier(APP_ACCESS_TOKEN_KEY)
            Identifier.IDFA -> getAndroidId()
            Identifier.IDFV -> getAndroidId()
            Identifier.APP_ID -> getGradleIdentifier(APP_ID_KEY)
            Identifier.COUNTRY_CODE -> context.detectCountryCodeFromSimOrNetworkOrLocale()
        }
    }

    private fun getAndroidId(): String =
        Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)

    private fun getGradleIdentifier(key: String): String {
        return try {
            val ai = context.packageManager.getApplicationInfo(
                context.packageName,
                PackageManager.GET_META_DATA
            )
            ai.metaData[key] as String? ?: ""
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            ""
        }
    }

}