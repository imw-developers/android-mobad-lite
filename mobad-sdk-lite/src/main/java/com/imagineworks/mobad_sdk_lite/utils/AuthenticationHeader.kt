package com.imagineworks.mobad_sdk_lite.utils

class AuthenticationHeader : HashMap<String, String>() {
}

internal fun getAuthenticationHeaderMap(token: String): AuthenticationHeader =
    AuthenticationHeader().apply {
        put("Authorization", "Bearer $token")
    }