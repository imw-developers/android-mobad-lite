package com.imagineworks.mobad_sdk_lite.utils

import android.content.Context
import android.content.ContextWrapper
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.telephony.TelephonyManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner

internal fun Context.detectCountryCodeFromSimOrNetworkOrLocale(defaultCountryIsoCode: String = "LB"): String {

    fun detectSIMCountry(): String? {
        return try {
            val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            telephonyManager.simCountryIso.uppercase().ifEmpty { null }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun detectNetworkCountry(): String? {
        return try {
            val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            telephonyManager.networkCountryIso.uppercase().ifEmpty { null }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun detectLocaleCountry(): String? {
        return try {
            val localeCountryISO = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                resources.configuration.locales.get(0).country
            } else {
                resources.configuration.locale.country
            }
            localeCountryISO.uppercase().ifEmpty { null }
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    detectSIMCountry()?.let {
        return it
    }

    detectNetworkCountry()?.let {
        return it
    }

    detectLocaleCountry()?.let {
        return it
    }

    return defaultCountryIsoCode
}

internal fun Context.isNetworkAvailable(): Boolean {
    var isConnected = false
    val connectivityManager =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val network = connectivityManager.activeNetwork ?: return false
        val networkCapabilities =
            connectivityManager.getNetworkCapabilities(network) ?: return false
        isConnected = when {
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
            else -> false
        }
    } else {
        connectivityManager.run {
            connectivityManager.activeNetworkInfo?.let {
                isConnected = it.isConnected
            }
        }
    }
    return isConnected
}

internal fun Context?.getLifecycle(): Lifecycle? {
    var context: Context? = this
    while (true) {
        when (context) {
            is LifecycleOwner -> return context.lifecycle
            !is ContextWrapper -> return null
            else -> context = context.baseContext
        }
    }
}