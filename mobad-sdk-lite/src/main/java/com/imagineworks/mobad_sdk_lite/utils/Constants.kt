package com.imagineworks.mobad_sdk_lite.utils

internal const val PREF_NAME = "com.imagineworks.mobad_sdk_lite"
internal const val BASE_URL = "http://20.203.74.44/api/v1/"
internal const val PREFERENCE_KEY_USER = "user"
internal const val APP_INSTANCE_ID = "appInstanceId"
internal const val FALL_BACK_COUNTRY_CODE = "fallBackCountryCode"
internal const val APP_ID_KEY = "app_id"
internal const val APP_ACCESS_TOKEN_KEY = "app_access_token"
internal const val TAG = "MobAd-Lite"
internal const val HTTP_REQUEST_TIMEOUT_SECONDS = 600L
internal const val NETWORK_UNREACHABLE = "Network is unreachable"
internal const val CONNECT_TIME_OUT = "Connect time out"