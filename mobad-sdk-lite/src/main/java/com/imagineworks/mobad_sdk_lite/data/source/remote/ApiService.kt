package com.imagineworks.mobad_sdk_lite.data.source.remote

import com.imagineworks.mobad_sdk_lite.data.model.*
import com.imagineworks.mobad_sdk_lite.utils.APP_INSTANCE_ID
import com.imagineworks.mobad_sdk_lite.utils.AuthenticationHeader
import com.imagineworks.mobad_sdk_lite.utils.FALL_BACK_COUNTRY_CODE
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiService {
    @POST(ApiEndPoint.INIT_USER)
    suspend fun initUser(
        @HeaderMap authenticationHeader: AuthenticationHeader,
        @Body initUserRequest: InitUserRequest
    ): Response<ApiResponse<User>>

    @GET(ApiEndPoint.REFRESH_USER_TOKEN)
    suspend fun refreshUserToken(
        @HeaderMap authenticationHeader: AuthenticationHeader
    ): Response<ApiResponse<RefreshUserTokenResponse>>

    @GET(ApiEndPoint.GET_AD)
    suspend fun getAd(
        @HeaderMap authenticationHeader: AuthenticationHeader,
        @Query(APP_INSTANCE_ID) appInstanceId: String,
        @Query(FALL_BACK_COUNTRY_CODE) fallBackCountryCode: String,
    ): Response<ApiResponse<Ad>>

    @POST(ApiEndPoint.AD_ACTION)
    suspend fun sendAdAction(
        @HeaderMap authenticationHeader: AuthenticationHeader,
        @Body adActionRequest: AdAction
    ): Response<ApiResponse<SendAdActionResponse>>

    @POST(ApiEndPoint.UPDATE_USER_PROFILE)
    suspend fun updateAdServiceStatus(
        @HeaderMap authenticationHeader: AuthenticationHeader,
        @Query(APP_INSTANCE_ID) appInstanceId: String,
        @Body adStatus: Boolean
    ):Response<ApiResponse<UpdateAdServiceStatusResponse>>
}