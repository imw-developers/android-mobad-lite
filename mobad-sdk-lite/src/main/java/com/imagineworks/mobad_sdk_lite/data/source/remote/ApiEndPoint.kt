package com.imagineworks.mobad_sdk_lite.data.source.remote

internal object ApiEndPoint {
    const val INIT_USER = "appInstanceService/app/register/initUser"
    const val GET_AD = "adService/ad/getAd"
    const val AD_ACTION = "statisticService/action/register"
    const val REFRESH_USER_TOKEN = "appInstanceService/profile/refreshUserToken"
    const val UPDATE_USER_PROFILE = "appInstanceService/profile/updateProfile"
}