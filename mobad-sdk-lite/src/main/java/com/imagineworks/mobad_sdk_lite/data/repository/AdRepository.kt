package com.imagineworks.mobad_sdk_lite.data.repository

import com.imagineworks.mobad_sdk_lite.data.model.*

interface AdRepository {
    suspend fun getAd(user: User): DataResponse<Ad>

    suspend fun sendAdAction(
        userAccessToken: String,
        adActionRequest: AdAction
    ): DataResponse<SendAdActionResponse>
}