package com.imagineworks.mobad_sdk_lite.data.source.local

import android.content.Context
import com.imagineworks.mobad_sdk_lite.data.model.User
import java.util.Date

class LocalDataSourceImpl(private val context: Context) : LocalDataSource {

    private val mobAdSharedPreferences
        get() = MobAdSharedPreferences.getInstance(context)

    override fun saveUser(user: User) =
        mobAdSharedPreferences.saveUser(
            user.copy(
                tokenLifeTimeInMillis = Date().time + user.tokenLifeTimeInMillis
            )
        )

    override fun getUser(): User =
        mobAdSharedPreferences.getUser()
}