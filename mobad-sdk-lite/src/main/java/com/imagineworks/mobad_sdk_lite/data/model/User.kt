package com.imagineworks.mobad_sdk_lite.data.model



data class InitUserRequest(
    val idfa: String,
    val idfv: String,
    val appId: String,
    val countryCode: String,
    val defaultLanguage: String
)

data class User(
    val id: String = "",
    val countryId: String = "",
    var token: String = "",
    var tokenLifeTimeInMillis: Long = 0L,
    var isOptedOut: Boolean = true
)

data class RefreshUserTokenResponse(
    var token: String = "",
    var tokenLifeTimeInMillis: Long = 0L
)
data class UpdateAdServiceStatusResponse(
    val isOptedOut: Boolean
)

