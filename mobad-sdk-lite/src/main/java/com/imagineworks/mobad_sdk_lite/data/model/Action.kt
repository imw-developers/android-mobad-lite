package com.imagineworks.mobad_sdk_lite.data.model

data class AdAction(
    val actionKey: String, //action
    val campaignId: String, //adID
    val tokenKey: String // actionToken
)

data class SendAdActionResponse(
    val data: String = "",
    val httpStatus: Int = 0
)