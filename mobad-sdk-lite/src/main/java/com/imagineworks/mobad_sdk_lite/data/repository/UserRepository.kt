package com.imagineworks.mobad_sdk_lite.data.repository

import com.imagineworks.mobad_sdk_lite.data.model.*

interface UserRepository {

    suspend fun initUser(
        appAccessToken: String,
        initUserRequest: InitUserRequest
    ): DataResponse<User>

    suspend fun updateAdServiceStatus(user: User, status : Boolean):DataResponse<UpdateAdServiceStatusResponse>

    suspend fun refreshUserToken(token: String): DataResponse<RefreshUserTokenResponse>

    fun getUser(): User

    fun saveUser(user: User)
}