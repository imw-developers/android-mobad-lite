package com.imagineworks.mobad_sdk_lite.data.source.local

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.GsonBuilder
import com.imagineworks.mobad_sdk_lite.data.model.User
import com.imagineworks.mobad_sdk_lite.utils.PREFERENCE_KEY_USER
import com.imagineworks.mobad_sdk_lite.utils.PREF_NAME

class MobAdSharedPreferences private constructor() {
    companion object {
        private lateinit var sharedPreferences: SharedPreferences

        @Volatile
        private var instance: MobAdSharedPreferences? = null

        @Synchronized
        fun getInstance(context: Context): MobAdSharedPreferences {
            return instance ?: synchronized(this) {
                if (!::sharedPreferences.isInitialized)
                    sharedPreferences =
                        context.applicationContext.getSharedPreferences(
                            PREF_NAME,
                            Context.MODE_PRIVATE
                        )
                val instance = MobAdSharedPreferences()
                this.instance = instance
                instance
            }
        }
    }

    fun saveUser(user: User) {

        sharedPreferences.edit()
            .putString(
                PREFERENCE_KEY_USER,
                GsonBuilder().serializeNulls().create().toJson(user) ?: ""
            ).apply()

    }

    fun getUser(): User {
        val userJson = sharedPreferences.getString(PREFERENCE_KEY_USER, null)
        if (userJson.isNullOrEmpty())
            return User()
        return GsonBuilder().create().fromJson(userJson, User::class.java)

    }
}