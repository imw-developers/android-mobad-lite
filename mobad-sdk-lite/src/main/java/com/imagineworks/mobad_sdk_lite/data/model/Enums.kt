package com.imagineworks.mobad_sdk_lite.data.model

enum class Identifier{
    APP_ACCESS_TOKEN,
    IDFA,
    IDFV,
    APP_ID,
    COUNTRY_CODE
}

enum class Action{
    CLICK,
    VIEW,
    IMPRESSION,
    PLAY_QUARTER,
    PLAY_HALF,
    PLAY_FULL
}