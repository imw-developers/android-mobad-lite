package com.imagineworks.mobad_sdk_lite.data.source.remote

import com.google.gson.Gson
import com.imagineworks.mobad_sdk_lite.data.model.*
import com.imagineworks.mobad_sdk_lite.utils.*
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

object RemoteDataSourceImpl : RemoteDataSource {
    private val okHttpClient = OkHttpClient.Builder().apply {
        readTimeout(HTTP_REQUEST_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        connectTimeout(HTTP_REQUEST_TIMEOUT_SECONDS, TimeUnit.SECONDS)
    }.build()

    private val retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val retrofitClient by lazy {
        retrofit.create(ApiService::class.java)
    }

    override suspend fun initUser(
        appAccessToken: String,
        initUserRequest: InitUserRequest
    ): DataResponse<ApiResponse<User>> {
        return try {
            val initUserResponse = retrofitClient.initUser(
                getAuthenticationHeaderMap(appAccessToken),
                initUserRequest
            )
            getTheRequiredDataResponse(initUserResponse)
        } catch (ex: SocketTimeoutException) {
            DataResponse.FailureResponse(ex.cause?.message ?: ex.message ?: CONNECT_TIME_OUT)
        }
    }

    override suspend fun refreshUserToken(
        userAccessToken: String
    ): DataResponse<ApiResponse<RefreshUserTokenResponse>> {
        return try {
            val refreshUserTokenResponse = retrofitClient.refreshUserToken(
                getAuthenticationHeaderMap(userAccessToken)
            )
            getTheRequiredDataResponse(refreshUserTokenResponse)
        } catch (ex: SocketTimeoutException) {
            DataResponse.FailureResponse(ex.cause?.message ?: ex.message ?: CONNECT_TIME_OUT)
        }
    }

    override suspend fun getAd(
        user: User
    ): DataResponse<ApiResponse<Ad>> {
        return try {
            val getAdResponse = retrofitClient.getAd(
                getAuthenticationHeaderMap(user.token),
                user.id,
                user.countryId
            )
            getTheRequiredDataResponse(getAdResponse)
        } catch (ex: SocketTimeoutException) {
            DataResponse.FailureResponse(ex.cause?.message ?: ex.message ?: CONNECT_TIME_OUT)
        }
    }

    override suspend fun sendAdAction(
        userAccessToken: String,
        adActionRequest: AdAction
    ): DataResponse<ApiResponse<SendAdActionResponse>> {
        return try {
            val sendAdActionResponse = retrofitClient.sendAdAction(
                getAuthenticationHeaderMap(userAccessToken),
                adActionRequest
            )
            getTheRequiredDataResponse(sendAdActionResponse)
        } catch (ex: SocketTimeoutException) {
            DataResponse.FailureResponse(ex.cause?.message ?: ex.message ?: CONNECT_TIME_OUT)
        }
    }

    override suspend fun updateAdServiceStatus(
        user: User,
        status: Boolean
    ): DataResponse<ApiResponse<UpdateAdServiceStatusResponse>> {
        return  try {
            val updateAdStatusResponse = retrofitClient.updateAdServiceStatus(getAuthenticationHeaderMap(user.token),user.id,status)
            getTheRequiredDataResponse(updateAdStatusResponse)
        }catch (ex: SocketTimeoutException){
            DataResponse.FailureResponse(ex.cause?.message ?: ex.message ?: CONNECT_TIME_OUT)
        }

    }


    private fun <T> getTheRequiredDataResponse(response: Response<ApiResponse<T>>): DataResponse<ApiResponse<T>> {
        val checkError = checkResponseErrors(response)
        return if (!checkError.isNullOrEmpty()) {
            DataResponse.FailureResponse(checkError)
        } else if (response.isSuccessful)
            if (response.body() != null)
                DataResponse.SuccessResponse(response.body()!!)
            else
                DataResponse.FailureResponse(response.message())
        else
            DataResponse.FailureResponse(response.message())
    }

    private fun <T> checkResponseErrors(
        response: Response<ApiResponse<T>>
    ): String? {
        val responseBody = response.body()
        if (response.isSuccessful) {
            return if (responseBody?.hasError() == true) {
                "Error " + responseBody.statusCode.toString() + ": " + responseBody.errorMessage()
            } else {
                null
            }
        } else {
            val responseFromError = response.errorBody()?.string()?.let { errorBodyString ->
                try {
                    Gson().fromJson(errorBodyString, ApiResponse::class.java)
                } catch (e: Exception) {
                    null
                }
            }
            return if (responseFromError?.hasError() == true) {
                "Error " + responseFromError.statusCode.toString() + ": " + responseFromError.errorMessage()
            } else
                null
        }
    }
}