package com.imagineworks.mobad_sdk_lite.data.model

import com.google.gson.annotations.SerializedName

data class Ad(
    val id: String? = null,
    val channel: String? = null,
    val contentType: String? = null,
    val showAdvertiserName: Boolean? = null,
    val advertiser: Advertiser? = Advertiser(),
    val showBubbleOnAndroid: Boolean? = null,
    val actionTokens: List<String> = listOf(),
    val content: List<Content> = listOf(),
    val mediaBaseUrl: String = "https://mobadserversdata.blob.core.windows.net/files"
)

data class Advertiser(
    val id: String? = null,
    val imagePath: String? = null,
    val name: String? = null
)

data class ContentDescription(
    val title: String? = null,
    val description: String? = null,
    val buttonTitle: String? = null,
    val url: String? = null
)

data class Content(
    val id: String? = null,
    @SerializedName("content") val contentDescription: ContentDescription? = ContentDescription(),
    val thumbnail: String? = null,
    val mediaPath: String? = null
)