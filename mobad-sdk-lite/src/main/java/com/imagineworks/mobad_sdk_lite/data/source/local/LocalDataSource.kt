package com.imagineworks.mobad_sdk_lite.data.source.local


import com.imagineworks.mobad_sdk_lite.data.model.User

interface LocalDataSource {
    fun saveUser(user: User)

    fun getUser(): User
}