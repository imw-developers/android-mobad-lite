package com.imagineworks.mobad_sdk_lite.data.model

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName

// from data layer to manager layer
sealed class DataResponse<out R> {
    data class SuccessResponse<out T>(val data: T) : DataResponse<T>()
    data class FailureResponse(val errorString: String) : DataResponse<Nothing>()
}

class ApiResponse<T> {
    val message: String? = null

    val data: T? = null

    @SerializedName("status")
    val statusCode: Int? = null

    fun toJSON() = GsonBuilder().serializeNulls().create().toJson(this) ?: ""

}

fun <T> ApiResponse<T>.hasError() = !(200..299).contains(statusCode)

fun <T> ApiResponse<T>.errorMessage() = message