package com.imagineworks.mobad_sdk_lite.data.source.remote

import com.imagineworks.mobad_sdk_lite.data.model.*


interface RemoteDataSource {
    suspend fun initUser(
        appAccessToken: String,
        initUserRequest: InitUserRequest
    ): DataResponse<ApiResponse<User>>

    suspend fun refreshUserToken(
        userAccessToken: String
    ): DataResponse<ApiResponse<RefreshUserTokenResponse>>

    suspend fun getAd(
        user: User
    ): DataResponse<ApiResponse<Ad>>

    suspend fun sendAdAction(
        userAccessToken: String,
        adActionRequest: AdAction
    ): DataResponse<ApiResponse<SendAdActionResponse>>

    suspend fun updateAdServiceStatus(
        user: User,
        status: Boolean
    ): DataResponse<ApiResponse<UpdateAdServiceStatusResponse>>
}