package com.imagineworks.mobad_sdk_lite.data.repository

import android.util.Log
import com.imagineworks.mobad_sdk_lite.data.model.*
import com.imagineworks.mobad_sdk_lite.data.source.local.LocalDataSource
import com.imagineworks.mobad_sdk_lite.data.source.remote.RemoteDataSource
import com.imagineworks.mobad_sdk_lite.data.source.remote.RemoteDataSourceImpl
import com.imagineworks.mobad_sdk_lite.utils.TAG

class MobAdRepository(
    private val localDataSource: LocalDataSource,
    private val remoteDataSource: RemoteDataSource = RemoteDataSourceImpl
) : UserRepository, AdRepository {

    override suspend fun initUser(
        appAccessToken: String,
        initUserRequest: InitUserRequest
    ): DataResponse<User> {
        val initUserResponse = remoteDataSource.initUser(appAccessToken, initUserRequest)
        return requiredResponseFromApiResponse(initUserResponse)
    }

    override suspend fun updateAdServiceStatus(
        user: User,
        status: Boolean
    ): DataResponse<UpdateAdServiceStatusResponse> {
        val updateAdStatusResponse = remoteDataSource.updateAdServiceStatus(user,status)
        return requiredResponseFromApiResponse(updateAdStatusResponse)

    }

    override suspend fun refreshUserToken(token: String): DataResponse<RefreshUserTokenResponse> {
        val refreshUserTokenResponse = remoteDataSource.refreshUserToken(token)
        return requiredResponseFromApiResponse(refreshUserTokenResponse)
    }

    override fun getUser(): User = localDataSource.getUser()

    override fun saveUser(user: User) = localDataSource.saveUser(user)

    override suspend fun getAd(user: User): DataResponse<Ad> {
        val getAdResponse = remoteDataSource.getAd(user)
        return requiredResponseFromApiResponse(getAdResponse)
    }

    override suspend fun sendAdAction(
        userAccessToken: String,
        adActionRequest: AdAction
    ): DataResponse<SendAdActionResponse> {
        val sendAdActionResponse =
            remoteDataSource.sendAdAction(userAccessToken, adActionRequest)
        return requiredResponseFromApiResponse(sendAdActionResponse)
    }

    private fun <T> requiredResponseFromApiResponse(
        response: DataResponse<ApiResponse<T>>
    ): DataResponse<T> {
        return when (response) {
            is DataResponse.FailureResponse -> {
                loge(response.errorString)
                response
            }
            is DataResponse.SuccessResponse -> {
                if (response.data.data != null)
                    DataResponse.SuccessResponse(response.data.data)
                else {
                    loge(response.data.message ?: "")
                    DataResponse.FailureResponse(response.data.message ?: "")
                }
            }
        }
    }

    private fun loge(errorMessage: String) = Log.e(TAG, errorMessage)

}