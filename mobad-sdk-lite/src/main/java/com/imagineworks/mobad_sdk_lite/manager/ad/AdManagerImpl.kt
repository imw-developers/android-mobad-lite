package com.imagineworks.mobad_sdk_lite.manager.ad

import com.imagineworks.mobad_sdk_lite.data.model.*
import com.imagineworks.mobad_sdk_lite.data.repository.AdRepository

class AdManagerImpl(
    private val adRepository: AdRepository
) : AdManager {

    override suspend fun getAd(user: User): DataResponse<Ad> {
        return adRepository.getAd(
            user
        )
    }

    override suspend fun sendAdAction(userAccessToken: String, adAction: AdAction) {
         adRepository.sendAdAction(userAccessToken,adAction)
    }

}