package com.imagineworks.mobad_sdk_lite.manager.ad

import com.imagineworks.mobad_sdk_lite.data.model.Ad
import com.imagineworks.mobad_sdk_lite.data.model.DataResponse
import com.imagineworks.mobad_sdk_lite.data.model.AdAction
import com.imagineworks.mobad_sdk_lite.data.model.User

interface AdManager {
    suspend fun getAd(user: User): DataResponse<Ad>

    suspend fun sendAdAction(userAccessToken:String,adAction: AdAction)
}