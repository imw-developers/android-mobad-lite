package com.imagineworks.mobad_sdk_lite.manager.user

import com.imagineworks.mobad_sdk_lite.data.model.*
import com.imagineworks.mobad_sdk_lite.data.repository.UserRepository
import java.util.Date

class UserManagerImpl(
    private val userRepository: UserRepository,
) : UserManager {

    private var user: User = User()

    override suspend fun initUser(
        appAccessToken: String,
        initUserRequest: InitUserRequest
    ): DataResponse<User> {
        val response = userRepository.initUser(
            appAccessToken,
            initUserRequest
        )

        if (response is DataResponse.SuccessResponse) {
            saveUser(response.data)
            user = response.data
        }

        return response
    }

    override suspend fun updateAdServiceStatus(
        userToBeUpdated: User,
        status: Boolean
    ): DataResponse<UpdateAdServiceStatusResponse> {
        val response = userRepository.updateAdServiceStatus(userToBeUpdated,status)

        if (response is DataResponse.SuccessResponse){
            user = getUser()
            user.isOptedOut = response.data.isOptedOut
            saveUser(user)
        }
        return response

    }

    override fun isUserInitialized(): Boolean {
        user = getUser()
        return user.token.isNotEmpty()
    }

    override fun isUserOptedOut(): Boolean {
        user = getUser()
        return  user.isOptedOut
    }

    override suspend fun getCurrentUserResponse(): DataResponse<User> {
        return if (isTokenExpired())
            refreshUserToken()
        else
            DataResponse.SuccessResponse(user)
    }

    private fun isTokenExpired(): Boolean =
        Date().time >= user.tokenLifeTimeInMillis

    private suspend fun refreshUserToken(): DataResponse<User> {
        return when (val refreshUserResponse = userRepository.refreshUserToken(user.token)) {
            is DataResponse.FailureResponse -> refreshUserResponse
            is DataResponse.SuccessResponse -> {
                user = user.copy(
                    token = refreshUserResponse.data.token,
                    tokenLifeTimeInMillis = refreshUserResponse.data.tokenLifeTimeInMillis
                )
                saveUser(user)
                DataResponse.SuccessResponse(user)
            }
        }
    }

    private fun saveUser(user: User) = userRepository.saveUser(user)

    override fun getUserAccessToken(): String = user.token

    private fun getUser(): User = userRepository.getUser()
}