package com.imagineworks.mobad_sdk_lite.manager.user

import com.imagineworks.mobad_sdk_lite.data.model.*

interface UserManager {
    suspend fun initUser(
        appAccessToken: String,
        initUserRequest: InitUserRequest
    ): DataResponse<User>

    suspend fun updateAdServiceStatus(userToBeUpdated: User, status: Boolean):DataResponse<UpdateAdServiceStatusResponse>

    suspend fun getCurrentUserResponse(): DataResponse<User>

    fun getUserAccessToken(): String


    fun isUserInitialized(): Boolean

    fun isUserOptedOut(): Boolean
}